import { AbstractControl, FormArray, FormGroup, ValidatorFn } from '@angular/forms';

export interface DependentValidatorBuilder {
  affectedControls(): AbstractControl[];
  validate(): boolean;
  inverse(): DependentValidatorBuilder;
}

export interface DependentValidatorBuilderRoot<T extends FormGroup | FormArray> {
  all(firstBuilder: DependentValidatorBuilder, secondBuilder: DependentValidatorBuilder,
    ...builders: DependentValidatorBuilder[]): DependentValidatorBuilder;

  any(firstBuilder: DependentValidatorBuilder, secondBuilder: DependentValidatorBuilder,
    ...builders: DependentValidatorBuilder[]): DependentValidatorBuilder;

  validator(predicate: (value: any, form: T) => boolean,
    firstPath: string | number | ((form: T) => AbstractControl),
    ...paths: (string | number)[]): DependentValidatorBuilder;
}

export interface DependentValidator {
  reset(): void;
  applyValidator(path: string | string[], firstValidator: ValidatorFn, ...validators: ValidatorFn[]): this;
  createValidator(firstValidator: ValidatorFn, ...validators: ValidatorFn[]): ValidatorFn;
}

export type DependentValidatorBuilderFactory<T extends FormGroup | FormArray>
  = (builder: DependentValidatorBuilderRoot<T>) => DependentValidatorBuilder;

export type DependentValidatorFactory<T extends FormGroup | FormArray>
  = (builderFactory: DependentValidatorBuilderFactory<T>) => DependentValidator;
