import { ReplaySubject, Subject } from 'rxjs';
import { shareReplay, withLatestFrom } from 'rxjs/operators';

import type { ControlValueAccessor } from "@angular/forms";
import type { Subscription } from "rxjs";

export class DeferredValueAccessor implements ControlValueAccessor {

  protected readonly subscriptions: Subscription[] = [];

  readonly #accessor = new Subject<ControlValueAccessor>();

  set valueAccessor(accessor: ControlValueAccessor) {
    this.#accessor.next(accessor);
  }

  readonly #writeValue = new ReplaySubject();
  readonly #registerOnChange = new ReplaySubject();
  readonly #registerOnTouched = new ReplaySubject();
  readonly #setDisabledState = new ReplaySubject<boolean>();

  constructor() {
    const accessorSbj = this.#accessor.pipe(shareReplay(1));

    this.subscriptions.push(
      this.#writeValue.pipe(withLatestFrom(accessorSbj)).subscribe(([val, accessor]) => accessor?.writeValue(val)),
      this.#registerOnChange.pipe(withLatestFrom(accessorSbj)).subscribe(([val, accessor]) => accessor?.registerOnChange(val)),
      this.#registerOnTouched.pipe(withLatestFrom(accessorSbj)).subscribe(([val, accessor]) => accessor?.registerOnTouched(val)),
      this.#setDisabledState.pipe(withLatestFrom(accessorSbj)).subscribe(([val, accessor]) => accessor?.setDisabledState(val)),
    );
  }

  writeValue(obj: any): void {
    this.#writeValue.next(obj);
  }

  registerOnChange(fn: any): void {
    this.#registerOnChange.next(fn);
  }

  registerOnTouched(fn: any): void {
    this.#registerOnTouched.next(fn);
  }

  setDisabledState(isDisabled: boolean): void {
    this.#setDisabledState.next(isDisabled);
  }
}
