export * from 'ngx-reactive-form/utils/composite-validator';
export * from 'ngx-reactive-form/utils/deferred-value-accessor';
export * from 'ngx-reactive-form/utils/provide-value-accessor';
export * from './lib/lazy';
export * from './lib/tap-controls';
export * from './lib/traverse-control';

export function uniq<T>(items: T[]): T[] {
  return (items || []).filter((val, idx, self) => val && self.indexOf(val) === idx);
}
