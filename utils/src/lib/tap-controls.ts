import { AbstractControl, FormArray, FormGroup } from '@angular/forms';

function getElements(ctrls: Record<string, AbstractControl> | AbstractControl[]) {
  return Array.isArray(ctrls) ? ctrls :
    typeof ctrls === 'object' && Object.values(ctrls) || [];
}

export function tapControls(root: FormGroup | FormArray, action: (control: AbstractControl) => void, updateState: boolean) {
  for (const ctrl of getElements(root && root.controls)) {
    if (ctrl instanceof FormGroup || ctrl instanceof FormArray) {
      tapControls(ctrl, action, updateState);
      continue;
    }

    if (typeof action === 'function') {
      action(ctrl);
    }

    if (updateState) {
      ctrl.updateValueAndValidity();
    }
  }
}
