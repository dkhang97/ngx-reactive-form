export function lazy<T>(factory: () => T, destroy?: (value: T) => void) {
  let init = false;
  let lVal: T;
  let err: any;

  function value() {
    if (!init) {
      init = true;
      try {
        lVal = factory();
      } catch (e) { err = e; }
    }
    if (err) { throw err; }
    return lVal;
  }

  value.reset = () => {
    if (init) {
      err = undefined;
      if (typeof destroy === 'function') {
        destroy(lVal);
      }
      init = false;
    }
  };

  value.isInit = () => init;

  return value;
}
