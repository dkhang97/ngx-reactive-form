import { AbstractControl, FormArray, FormGroup } from '@angular/forms';

export function traverseControl(control: AbstractControl, ...ps: (string | number)[]): AbstractControl {
    if (ps.length === 0 || !control) {
        return control;
    }

    if (control instanceof FormGroup || control instanceof FormArray) {
        const [p, ...lps] = ps;
        return traverseControl(control.controls[p], ...lps);
    }
}
