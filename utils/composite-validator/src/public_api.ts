import { AbstractControl, AsyncValidatorFn, FormArray, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { forkJoin, from, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

interface ErrorField {
  key: string | number;
  err: ValidationErrors;
}


function mergeError(errorArray: ErrorField[]): Record<string, ValidationErrors> {
  const sanitizedErrors = errorArray?.filter(({ err }) => Object.keys(err || {}).length);

  return !sanitizedErrors?.length ? null
    : Object.assign({}, ...sanitizedErrors.map(({ key, err }) => ({ [key]: err })));
}

function loopControl<T>(control: AbstractControl, selector: (pairArray: {
  key: string | number, control: AbstractControl
}[]) => T): T {
  if (control instanceof FormGroup) {
    return selector(Object.entries(control.controls)
      .map(([key, ctrl]) => ({ key, control: ctrl })));
  } else if (control instanceof FormArray) {
    return selector(control.controls
      .map((ctrl, key) => ({ key, control: ctrl })))
  }
}

export function compositeValidator(): ValidatorFn {
  const validator: ValidatorFn = (ctrl: AbstractControl) => {
    const vl = ctrl?.validator;

    if (typeof vl === 'function') {
      return vl(ctrl);
    }

    const errAry = loopControl(ctrl, pairs => pairs.reduce((acc, { key, control }) =>
      [...acc, { key, err: validator(control) }], [] as ErrorField[]));

    return mergeError(errAry);
  }

  return control => {
    if (control instanceof FormGroup || control instanceof FormArray) {
      return validator(control);
    }
  }
}

export function compositeAsyncValidator(): AsyncValidatorFn {
  const validator: AsyncValidatorFn = (ctrl: AbstractControl) => {
    const vl = ctrl?.asyncValidator;

    if (typeof vl === 'function') {
      return vl(ctrl);
    }

    const asyncErrAry = loopControl(ctrl, pairs => pairs.reduce((acc, { key, control }) => {
      const asyncErr = validator(control);

      if (asyncErr) {
        return [...acc, from(asyncErr).pipe(
          map(err => ({ key, err })),
        )];
      }

      return acc;
    }, [] as Observable<ErrorField>[]));

    return !asyncErrAry?.length ? of(null)
      : forkJoin(asyncErrAry).pipe(
        map(errAry => mergeError(errAry)),
      );
  }

  return control => {
    if (control instanceof FormGroup || control instanceof FormArray) {
      return validator(control);
    }
  }
}
