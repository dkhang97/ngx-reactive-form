import { FactoryProvider, forwardRef, Type } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export type ProvideValueAccessorFieldSelector<C extends object> = ((component: C) => ControlValueAccessor) | {
  [K in keyof C]: C[K] extends ControlValueAccessor ? K : never
}[keyof C];


export function provideValueAccessor(fn: () => Type<ControlValueAccessor>): FactoryProvider;
export function provideValueAccessor<C extends object>(fn: () => Type<C>,
  fieldSelector: ProvideValueAccessorFieldSelector<C>): FactoryProvider;
export function provideValueAccessor<C extends object>(fn: () => Type<C>,
  fieldSelector?: ProvideValueAccessorFieldSelector<C>): FactoryProvider {
  return {
    provide: NG_VALUE_ACCESSOR,
    useFactory: (component: C) => (fieldSelector === undefined || fieldSelector === null) ? component
      : (accessor => ({
        registerOnChange: cvaFn => accessor().registerOnChange(cvaFn),
        registerOnTouched: cvaFn => accessor().registerOnTouched(cvaFn),
        writeValue: value => accessor().writeValue(value),
        setDisabledState: isDisabled => accessor().setDisabledState?.(isDisabled),
      }) as ControlValueAccessor)(() => typeof fieldSelector === 'function' ? fieldSelector(component)
        : component[fieldSelector] as unknown as ControlValueAccessor),
    deps: [forwardRef(fn)],
    multi: true,
  };
}
