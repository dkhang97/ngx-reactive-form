
# ngx-reactive-form

[![package version](https://img.shields.io/npm/v/ngx-reactive-form.svg?style=flat-square)](https://npmjs.org/package/ngx-reactive-form)
[![package downloads](https://img.shields.io/npm/dm/ngx-reactive-form.svg?style=flat-square)](https://npmjs.org/package/ngx-reactive-form)
[![package license](https://img.shields.io/npm/l/ngx-reactive-form.svg?style=flat-square)](https://npmjs.org/package/ngx-reactive-form)

## Installation

```bash
npm i -S ngx-reactive-form
```

## Usage

```ts
import { Component } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { ExtendedFormBuilder } from 'ngx-reactive-form';

@Component({
  selector: 'app-todo',
  template: '',
})
export class TodoComponent {
  readonly todoFormExtra = this.extendedFb.build<FormGroup>((fb, validatorFactory) => {
    // Create dependent validator for 'priority' field
    const highPriorityValidator = validatorFactory(f =>
      f.validator(priority => ['URGENT', 'HIGH'].includes(priority), 'priority'));

    return fb.group({
      content: ['', Validators.required],
      priority: ['', Validators.required],
      // Expire Date is required if priority is HIGH or URGENT
      expireDate: ['', highPriorityValidator.createValidator(Validators.required)],
    });
  });

  // Return the FormGroup for template HTML binding
  readonly form: FormGroup = this.todoFormExtra.form;

  constructor(
    private extendedFb: ExtendedFormBuilder,
  ) { }

  makeDirty() {
    // Mark all controls inside the form as dirty
    this.todoFormExtra.formUtils.makeDirty();
  }
}

```
