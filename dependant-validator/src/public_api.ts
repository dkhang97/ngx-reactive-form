import { AbstractControl, FormArray, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import {
  DependentValidator,
  DependentValidatorBuilder,
  DependentValidatorBuilderFactory,
} from 'ngx-reactive-form/declarations';
import { lazy, traverseControl } from 'ngx-reactive-form/utils';
import { Subscription } from 'rxjs';

import { DependentValidatorBuilderRootImpl } from './lib/builder-root';

export class DependentValidatorImpl<T extends FormGroup | FormArray> implements DependentValidator {
  private builder = lazy(() => {
    if (typeof this.builderFn === 'function') {
      const builderRoot = new DependentValidatorBuilderRootImpl<T>(() => this.rootFn());
      return this.builderFn(builderRoot)
    }
  });

  private subscribedControls: AbstractControl[] = [];

  private subscription = lazy<Subscription[]>(() =>
    this.builder().affectedControls()

      .filter(item => !!item).map(item => item
        .valueChanges.subscribe(() => this.subscribedControls
          .forEach(control => control.updateValueAndValidity()))),
    subs => subs.forEach(s => s.unsubscribe())
  );

  constructor(
    private rootFn: () => T,
    private builderFn: DependentValidatorBuilderFactory<T>
  ) { }

  reset() {
    this.builder.reset();
    this.subscription.reset();
  }

  applyValidator(path: string | string[], ...validators: ValidatorFn[]) {
    const paths = Array.isArray(path) ? path : [path];
    const ctrl = traverseControl(this.rootFn(), ...paths);

    if (ctrl) {
      ctrl.setValidators(this.createValidator(...validators));
    }

    return this;
  }

  createValidator(...validators: ValidatorFn[]): ValidatorFn {
    const validate = Validators.compose(validators);
    let ctrlSubscribed = false;

    return ctrl => {
      let builder: DependentValidatorBuilder;

      if (!ctrlSubscribed) {
        ctrlSubscribed = true;
        if (this.subscribedControls.indexOf(ctrl) === -1) {
          this.subscribedControls.push(ctrl);
        }
      }

      if (!this.subscription.isInit() && this.rootFn()) {
        builder = this.builder();
        this.subscription();
      } else if (this.builder.isInit()) {
        builder = this.builder();
      }

      return (!builder || builder.validate()) ? validate(ctrl) : null;
    };
  }
}

