import { AbstractControl } from '@angular/forms';
import { DependentValidatorBuilder } from 'ngx-reactive-form/declarations';

export class DependentValidatorBuilderImpl {
  constructor(
    readonly affectedControls: () => AbstractControl[],
    readonly validate: () => boolean,
  ) { }

  inverse(): DependentValidatorBuilder {
    return new DependentValidatorBuilderImpl(
      this.affectedControls, () => !this.validate()
    );
  }
}

