import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { DependentValidatorBuilder, DependentValidatorBuilderRoot } from 'ngx-reactive-form/declarations';
import { lazy, traverseControl, uniq } from 'ngx-reactive-form/utils';

import { DependentValidatorBuilderImpl } from './builder';

export class DependentValidatorBuilderRootImpl<T extends FormGroup | FormArray>
  implements DependentValidatorBuilderRoot<T> {
  constructor(
    private form: () => T
  ) { }

  all(...builders: DependentValidatorBuilder[]): DependentValidatorBuilder {
    return new DependentValidatorBuilderImpl(
      () => uniq([].concat(builders.map(b => b.affectedControls()))),
      () => builders.every(b => b.validate()),
    );
  }

  any(...builders: DependentValidatorBuilder[]): DependentValidatorBuilder {
    return new DependentValidatorBuilderImpl(
      () => uniq([].concat(builders.map(b => b.affectedControls()))),
      () => builders.some(b => b.validate()),
    );
  }

  validator(predicate: (value: any, form: T) => boolean, firstPath: string | number | ((form: T) => AbstractControl),
    ...paths: (string | number)[]): DependentValidatorBuilder {
    const formFn = lazy(() => this.form());
    const ctrlFn = lazy(() => {
      if (typeof firstPath === 'string' || typeof firstPath === 'number') {
        return traverseControl(formFn(), firstPath, ...paths);
      } else if (typeof firstPath === 'function') {
        return firstPath(formFn());
      }
    });

    return new DependentValidatorBuilderImpl(() => [ctrlFn()], () => {
      const ctrl = ctrlFn();
      return ctrl && typeof predicate === 'function' && predicate(ctrl.value, formFn())
    });
  }
}
