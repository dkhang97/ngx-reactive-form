import { Injectable, Injector } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import {
  DependentValidator,
  DependentValidatorBuilderFactory,
  DependentValidatorFactory,
} from 'ngx-reactive-form/declarations';
import { lazy } from 'ngx-reactive-form/utils';

import { FormUtils } from './form-utils';

export class ExtendedFormBuilderResult<T extends FormGroup | FormArray> {
  protected readonly fb = this.injector.get(FormBuilder);
  private validators: DependentValidator[] = [];
  readonly #formUtils = lazy(() => new FormUtils(this.injector, () => this.form));
  readonly form = this.factory(this.fb, dv => {
    const v = this.formUtils.createDependentValidator(dv)
    this.validators.push(v);
    return v;
  });
  readonly #initValue = this.form.value;

  get formUtils() {
    return this.#formUtils();
  }

  constructor(
    protected readonly injector: Injector,
    private factory: (fb: FormBuilder,
      dp: (builderFn: DependentValidatorBuilderFactory<T>) => DependentValidator) => T,
  ) { }

  updateValidators() {
    this.validators.forEach(item => item.reset());
    this.formUtils.tapControls();
  }

  reset(options?: Parameters<(FormGroup | FormArray)['reset']>[1]) {
    this.form.reset(this.#initValue, options);
  }
}

@Injectable({
  providedIn: 'root'
})
export class ExtendedFormBuilder {

  constructor(
    protected readonly injector: Injector,
  ) { }

  build<T extends FormGroup | FormArray>(factory: (fb: FormBuilder,
    dependentValidatorFactory: DependentValidatorFactory<T>) => T) {
    return new ExtendedFormBuilderResult(this.injector, factory);
  }
}
