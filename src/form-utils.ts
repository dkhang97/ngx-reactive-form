import { InjectFlags, Injector } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormArray, FormGroup, NgControl, ValidatorFn } from '@angular/forms';
import { DependentValidator, DependentValidatorBuilderFactory } from 'ngx-reactive-form/declarations';
import { DependentValidatorImpl } from 'ngx-reactive-form/dependant-validator';
import { compositeAsyncValidator, compositeValidator, lazy, tapControls } from 'ngx-reactive-form/utils';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';


const subFormMap = new WeakMap<AbstractControl, FormUtils<any>[]>();

export class FormUtils<T extends FormGroup | FormArray> {
  readonly #root = lazy(() => {
    if (this.rootFn instanceof FormGroup || this.rootFn instanceof FormArray) {
      return this.rootFn;
    } else if (typeof this.rootFn === 'function') {
      return this.rootFn();
    }
  });
  readonly #compositeValidator = lazy((): ValidatorFn => {
    const v = compositeValidator();
    return () => v(this.root);
  });
  readonly #compositeAsyncValidator = lazy((): AsyncValidatorFn => {
    const v = compositeAsyncValidator();
    return () => v(this.root);
  });

  get root(): T {
    const r = this.#root;

    if (!r()) {
      r.reset();
    }

    return r() as any;
  }

  get compositeValidator() { return this.#compositeValidator(); }
  get compositeAsyncValidator() { return this.#compositeAsyncValidator(); }

  constructor(
    protected readonly injector: Injector,
    protected readonly rootFn: T | (() => T),
  ) { }

  tapControls(action?: (control: AbstractControl) => void, updateState = true) {
    tapControls(this.root, action, updateState);
  }

  createDependentValidator(builderFn: DependentValidatorBuilderFactory<T>): DependentValidator {
    return new DependentValidatorImpl(() => this.root, builderFn);
  }

  makePristine() {
    this.tapControls(ctrl => {
      ctrl.markAsPristine();
      ctrl.markAsUntouched();
      subFormMap.get(ctrl)?.forEach(subForm => subForm.makePristine());
    })
  }

  makeDirty() {
    this.tapControls(ctrl => {
      ctrl.markAsDirty();
      ctrl.markAsTouched();
      subFormMap.get(ctrl)?.forEach(subForm => subForm.makeDirty());
    });
  }

  /**
   * Consume this method on **NG_VALUE_ACCESSOR** `afterViewInit()`
   * ### Note
   * Add `ExtendedFormBuilder` to component providers
   *
   * @example
   * ```typescript
   *  ngAfterViewInit() {
   *   this.subscriptions.push(
   *    this.exForm.formUtils.registerSubFormValidator(),
   *   );
   *  }
   * ```
   */
  registerSubFormValidator() {
    const subscriptions: Subscription[] = [];

    const ctrl = this.injector.get(NgControl, null, InjectFlags.Self | InjectFlags.Optional)?.control;
    if (ctrl) {
      ctrl.validator ??= this.compositeValidator;
      ctrl.asyncValidator ??= this.compositeAsyncValidator;

      setTimeout(() => {
        this.root.updateValueAndValidity({ emitEvent: true });
      }, 0);

      subscriptions.push(ctrl.statusChanges.pipe(
        debounceTime(50),
      ).subscribe(() => {
        setTimeout(() => {
          this.root.updateValueAndValidity({ emitEvent: true });
        }, 0);
      }));

      let subForms = subFormMap.get(ctrl);
      if (!subForms) { subFormMap.set(ctrl, subForms = []); }
      subForms.push(this);
    }

    return new Subscription(() => {
      subscriptions.forEach(s => s.unsubscribe());
      if (ctrl) {
        const subForm = subFormMap.get(ctrl)?.filter(f => f !== this);
        if (subForm?.length) {
          subFormMap.set(ctrl, subForm);
        } else {
          subFormMap.delete(ctrl);
        }
      }
    });
  }
}
